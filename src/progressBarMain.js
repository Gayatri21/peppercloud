import React from 'react'

export const ProgressBarMain = () => {
  return (
    <div>
     <div className="progress-div">
    <div className="step-row">
      <div className="step-col">Status 1</div>
      <div className="step-col">New</div>
      <div className="step-col">Working</div>
      <div className="step-col">Nurturing</div>
      <div className="step-col">Converted</div>
    </div>
  </div></div>
  )
}
