import React from "react";
import "./style.css";

class Tasks extends React.Component {
  constructor() {
    super();
    this.state = {
      employeeData: [],
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    let employeeData = this.state.employeeData;
    let taskType = this.refs.taskType.value;
    let subject = this.refs.subject.value;
    let assignedTo = this.refs.assignedTo.value;
    let Status = this.refs.Status.value;


    let newEmployeeData = {
      taskType: taskType,
      subject: subject,
      assignedTo: assignedTo,
      Status: Status
    };
    employeeData.push(newEmployeeData);
    this.setState({
      employeeData: employeeData,
    });
    console.log("emp", employeeData);
    this.refs.myForm.reset();
  };

  
  handleDelete = (i) => {
    let employeeData = this.state.employeeData;
    employeeData.splice(i, 1);
    this.setState({
      employeeData: employeeData,
    });
  };

  render() {
    let employeeData = this.state.employeeData;  
  return (
    <div className="task-app">
      <form class="form-group" ref="myForm" >
        <h5>New Tasks</h5>
        <div className="row">
          <div className="col">
            <label for="sel1" className="mb-2">
              Select Task Type:
            </label>
            <select
              class="form-control"
              id="sel1"
              name="taskType"
              ref="taskType"
            
            >
              <option>To Do</option>
              <option>In Progress</option>
              <option>Completed</option>
              <option>Rejected</option>
            </select>
          </div>
          <div className="col">
            <label for="sel1" className="mb-2">
              Subject:
            </label>
            <input
              type="text"
              className="form-control"
              placeholder="Enter Subject"
              name="subject"
              ref="subject"
            
            />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <br />
            <label for="sel2" 
            className="mb-2"
             >
              Assigned To
            </label>
            <select 
            class="form-control" 
            id="sel2" 
            name="assignedTo" 
            ref="assignedTo" 
             >
              <option>Ravi Kumar</option>
              <option>Anand Pai</option>
              <option>Jayaram Ramesh</option>
              <option>Allan Gamez</option>
              <option>5</option>
            </select>
          </div>
          <div className="col">
            <br />
            <label for="sel2" className="mb-2">
              Status
            </label>
            <select
             class="form-control" 
            id="sel2"  
            name="Status" 
            ref="Status" 
            >
              <option>Not Started</option>
              <option>Pending</option>
              <option>Completed</option>
              <option>Rejected</option>
            </select>
          </div>
        </div>
        <div>
          <button className="btn btn-danger tasks-btn ">Cancel</button>
          <button className="btn btn-primary tasks-btn " type="submit" onClick={(e) => this.handleSubmit(e)} >Apply</button>
        </div>
      </form>

      <div>
      <hr />
      <h5>Task List</h5>

      <table class="table">
      
            <thead>
              <tr>
                <th>Task</th>
                <th>Status</th>
                <th>Assigned To</th>
                <th>Subject</th>
              </tr>
            </thead>

            <tbody>
              {employeeData.map((data) => (
                <tr>
                <td>{data.taskType}</td>
                  <td>{data.Status}</td>
                  <td>{data.assignedTo}</td>
                  <td>{data.subject}</td>
                </tr>
              ))}
            </tbody>
          </table>
          </div>
    </div>
  );
}
}

export default Tasks;
