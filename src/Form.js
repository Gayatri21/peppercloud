import React from "react";
import { Activity } from "./Activity";
import { ProgressBarMain } from "./progressBarMain";
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import "bootstrap/dist/css/bootstrap.min.css";

export const Form = () => {
  return (
    <div className="App">
      <ProgressBarMain />
      <div className="col-lg-12 ">
    
      <Tabs defaultActiveKey="home" id="uncontrolled-tab-example">
        <Tab
          eventKey="home"
          title="Activity"
          mountOnEnter // <<<
          unmountOnExit={false} // <<<
        >
        <Activity/>
        </Tab>
        <Tab
          eventKey="profile"
          title="Information"
          mountOnEnter // <<<
          unmountOnExit={false} // <<<
        >
        Thank you
        </Tab>
      </Tabs>
    </div>
      
    </div>
  );
};
