import React from "react";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import "bootstrap/dist/css/bootstrap.min.css";
import "./style.css";
import Tasks from "./Tasks";
import 'font-awesome/css/font-awesome.min.css'

export const Activity = () => {
  return (
    <div className="row main-div">
      <div className="col-lg-8 task-form">
        <Tabs defaultActiveKey="home" id="uncontrolled-tab-example">
          <Tab
            eventKey="LogCalls"
            title="LogCalls"
            mountOnEnter // <<<
            unmountOnExit={false} // <<<
          >
             <Tasks />
          </Tab>
          <Tab
            eventKey="Tasks"
            title="Tasks"
            mountOnEnter // <<<
            unmountOnExit={false} // <<<
          >
            <Tasks />
          </Tab>
          <Tab
            eventKey="Events"
            title="Events"
            mountOnEnter // <<<
            unmountOnExit={false} // <<<
          >
        
          </Tab>
          <Tab
            eventKey="MakeNote"
            title="Make Note"
            mountOnEnter // <<<
            unmountOnExit={false} // <<<
          >
            Make Note
          </Tab>
          <Tab
            eventKey="AddAttachments"
            title="Add Attachments"
            mountOnEnter // <<<
            unmountOnExit={false} // <<<
          >
            Add Attachments
          </Tab>
          <Tab
            eventKey="ActivitySet"
            title="Activity Set"
            mountOnEnter // <<<
            unmountOnExit={false} // <<<
          >
            Activity Set
          </Tab>
          <Tab
            eventKey="custom"
            title="Custom Field"
            mountOnEnter // <<<
            unmountOnExit={false} // <<<
          >
            Custom Field
          </Tab>
        </Tabs>
      </div>
      <div className="col-lg-4 list-div p-5">
        <div className="info-div">          
          <div className="col center">
          <div className="center"><i className="fa fa-angellist"></i></div>
            <h6 className="color-code1">Informed product Features</h6>
            <p>You Contacted with<span className="color-code"> Charles Gamez</span></p>
          </div>
        </div>
        <div className="info-div">          
          <div className="col center">
          <div className="center"><i className="fa fa-angellist"></i></div>
            <h6 className="color-code1">Informed product Features</h6>
            <p>You Contacted with<span className="color-code"> Charles Gamez</span></p>
          </div>
        </div>
        <div className="info-div">          
          <div className="col center">
          <div className="center"><i className="fa fa-angellist"></i></div>
            <h6 className="color-code1">Informed product Features</h6>
            <p>You Contacted with<span className="color-code"> Charles Gamez</span></p>
          </div>
        </div>
        <div className="info-div">          
          <div className="col center">
          <div className="center"><i className="fa fa-angellist"></i></div>
            <h6 className="color-code1">Informed product Features</h6>
            <p>You Contacted with<span className="color-code"> Charles Gamez</span></p>
          </div>
        </div>
        <div className="info-div">          
          <div className="col center">
          <div className="center"><i className="fa fa-angellist"></i></div>
            <h6 className="color-code1">Informed product Features</h6>
            <p>You Contacted with<span className="color-code"> Charles Gamez</span></p>
          </div>
        </div>
      </div>
    </div>
  );
};
